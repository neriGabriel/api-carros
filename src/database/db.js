const config = require('../../config/default.json');
const { Pool, Client } = require('pg');

const client =  new Client({
    user: config.db.user,
    host: config.db.host,
    database: config.db.database,
    password: config.db.password,
    port: config.db.port
});

const pool  = new Pool({
    user: config.db.user,
    host: config.db.host,
    database: config.db.database,
    password: config.db.password,
    port: config.db.port
})               
 
module.exports = {
    client, 
    pool,
    async connect() {
        await client.connect();
    },
    async disconnect() {
        await client.end();
    },
}

