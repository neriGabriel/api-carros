//LIBS IMPORTS
const config   = require('../config/default.json');
const express  = require('express')
const cors     = require('cors')
const routes   = require('./routes.js');

const app = express();

app.use(cors());

app.use(express.json());

app.use(routes);

app.listen(config.app.port, () => {
    console.log(`APP IS RUNNING SUCCESSFULLY on PORT ${config.app.port} AT VERSION: ${config.app.version}`);
});