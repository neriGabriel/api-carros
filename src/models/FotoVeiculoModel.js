const tableName = 'tb_fotos_veiculos';
const { client, pool} = require('../database/db');

module.exports = {
    /**
     * findUnique filter a unique row by primary key
     */
    async findUnique(response, parameters) {
        const id = parameters.params.id;
        return await pool.query(`SELECT * FROM ${tableName} WHERE fov_id = $1`, [id])
        .then(success =>  {
            return {
                type:       'success' ,
                message:    'SUCCESS in query exec' ,
                command:     success.command ,
                rowAsArray:  success.rowAsArray ,
                result:      ( success.rows.length == 1) ?  success.rows[0] : {}
            };
        })
        .catch(error =>  {
            return { 
                type:       'error' ,
                message:    'ERROR in query exec' ,
                command:     error.command ,
                rowAsArray:  error.rowAsArray ,
                result:      error.rows
            }
        });
    } , 

    /**
     *  findAll filter all results with no parameters
     */
    async findAll(response, parameters) {
        return await pool.query(`SELECT * FROM ${tableName}`)
        .then(success =>  {
            return {
                type:       'success' ,
                message:    'SUCCESS in query exec' ,
                command:     success.command ,
                rowAsArray:  success.rowAsArray ,
                result:      success.rows
            };
        })
        .catch(error =>  {
            return { 
                type:       'error' ,
                message:    'ERROR in query exec' ,
                command:     error.command ,
                rowAsArray:  error.rowAsArray ,
                result:      error.rows
            }
        });
    } , 

    /**
     *  insert data in table
     */
    async insert(response, parameters) {
        const { fov_montadora, fov_url_veiculo, fov_url_imagem } = parameters.body;

        return await pool.query(`INSERT INTO ${tableName} ( fov_montadora, fov_url_veiculo, fov_url_imagem ) 
                                 VALUES($1, $2, $3) RETURNING *`,[fov_montadora, fov_url_veiculo, fov_url_imagem])
        .then(success => {
            return {
                type:       'success' ,
                message:    'SUCCESS in query exec' ,
                command:     success.command ,
                rowAsArray:  success.rowAsArray ,
                result:      success.rows
            };
        })
        .catch(error => {
            return {
                type:       'error' ,
                message:    'ERROR in query exec, CODE: '+error.code,
                command:     error.command ,
                rowAsArray:  error.rowAsArray ,
                result:      error.rows
            };
        });  
    } ,

    /**
     *  edit data from table 
     */
    async edit(response, parameters) {
        const {fov_montadora, fov_url_veiculo, fov_url_imagem } = parameters.body;
        const fov_id = parameters.params.id;

        return await pool.query(`UPDATE ${tableName} SET fov_montadora = $1, fov_url_veiculo = $2, fov_url_imagem = $3 WHERE fov_id = $4 RETURNING *`,
                     [fov_montadora, fov_url_veiculo, fov_url_imagem, fov_id])
        .then(success => {
                        return {
                            type:       'success' ,
                            message:    'SUCCESS in query exec' ,
                            command:     success.command ,
                            rowAsArray:  success.rowAsArray ,
                            result:      success.rows
                        };
                    })
                    .catch(error => {
                        return {
                            type:       'error' ,
                            message:    'ERROR in query exec, CODE: '+error.code,
                            command:     error.command ,
                            rowAsArray:  error.rowAsArray ,
                            result:      error.rows
                        };
                    });  

    } ,

    async exclude(response, parameters) {
        const fov_id = parameters.params.id;

        return await pool.query(`DELETE FROM ${tableName} WHERE fov_id = $1`,[fov_id])
        .then(success => {
            return {
                type:       'success' ,
                message:    'SUCCESS in query exec' ,
                command:     success.command ,
                rowAsArray:  success.rowAsArray ,
                result:      success.rows
            };
        })
        .catch(error => {
            return {
                type:       'error' ,
                message:    'ERROR in query exec, CODE: '+error.code,
                command:     error.command ,
                rowAsArray:  error.rowAsArray ,
                result:      error.rows
            };
        }); 
    } ,
};