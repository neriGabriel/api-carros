const  {findUnique, findAll, insert, edit, exclude } = require('../models/FotoVeiculoModel');

module.exports = {
    async unique(request, response) {
        return response.json(await findUnique(response, request));
    } ,

    async index(request, response) {
        return response.json(await findAll(response));
    } ,

    async store(request, response) {
        return response.json(await insert(response, request));
    } ,

    async edit(request, response) {
        return response.json(await edit(response, request));
    } ,

    async delete(request, response) {
        return response.json(await exclude(response, request));
    }

}