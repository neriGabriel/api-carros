const { Router }                   = require('express');
const FotoVeiculoController        = require('./controllers/FotoVeiculoController');
const routes                       = Router();

routes.get('/fotoveiculo/:id'    , FotoVeiculoController.unique);
routes.get('/fotoveiculo'        , FotoVeiculoController.index);
routes.post('/fotoveiculo'       , FotoVeiculoController.store);
routes.put('/fotoveiculo/:id'    , FotoVeiculoController.edit);
routes.delete('/fotoveiculo/:id' , FotoVeiculoController.delete);

module.exports = routes;