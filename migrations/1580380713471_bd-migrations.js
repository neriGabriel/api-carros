/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = pgm => {
    pgm.createTable('tb_fotos_veiculos', {
        fov_id: {
            type:       'SERIAL' ,
            primaryKey: true , 
            notNull:    true
        } ,
        fov_montadora: {
            type:       'VARCHAR(100)',
            notNull:    true
        } , 
        fov_url_veiculo: {
            type:       'TEXT',
            notNull:    true
        } ,
        fov_url_imagem: {
            type:       'TEXT',
            notNull:    true
        }
    },{
        ifNotExists: true
    });
};

exports.down = pgm => {
    pgm.dropTable('tb_fotos_veiculos')
};
