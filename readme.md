# API CARROS

## Como rodar este projeto
 - baixe/clone o repositório
 - instala as libs/dependencias necessarias
 - dentro da pasta config na raiz do projeto estão todas as variveis de configuração de ambiente, altera-as conforme o necessário
 - rode as migrations
 - rode o projeto

## Rotas de aecsso api
 - findUnico: http://127.0.0.1:${port}/fotoveiculo/:id    (get)
 - findTodos: http://127.0.0.1:${port}/fotoveiculo/       (get)
 - inserir  : http://127.0.0.1:${port}/fotoveiculo/       (post)
 - editar   : http://127.0.0.1:${port}/fotoveiculo/:id    (put)
 - deletar  : http://127.0.0.1:${port}/fotoveiculo/:id    (delete)

## Estrutura do json body
 - inserir: {"fov_montadora": "fov_url_veiculo", "fov_url_imagem"}
 - edit: id deve estar na url, {"fov_montadora": "fov_url_veiculo", "fov_url_imagem"}
 
## Libs
 - [axios](https://github.com/axios/axios)
 - [config](https://www.npmjs.com/package/config)
 - [cors](https://www.npmjs.com/package/cors)
 - [express](https://expressjs.com/pt-br/)
 - [node-pg-migrate](https://github.com/salsita/node-pg-migrate)
 - [pg](https://node-postgres.com/)
 - [nodemon](https://www.npmjs.com/package/nodemon)

## Estrutura
 - config
   - default.json
 - migrations
 - src
 - controllers
 - database
 - models
 - core.js
 - routes.js  

## Comandos
 - yarn/npm install             => instala as libs necessárias
 - yarn/npm run migrate up/down => roda as migrations no banco de dados
 - yarn/npm run dev             => roda o projeto

